<?php
function http_post_json($url, $jsonStr)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization:秘钥',
            'Content-Length: ' . strlen($jsonStr)
        )
    );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return array($httpCode, $response);
}

function https_request($url, $data = null)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if (!empty($data)) {
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
}

$url = "机器人服务器外网ip/robot.php?do=remote";

$yjh = '我是一句话';
$wxts = '我是温馨提示';
$name = '女朋友的名字';

$qinghuaqiurl = 'https://api.youguo56.com/api/chp/'; //
$qinghuaapi = https_request($qinghuaqiurl);

$tianqiurl = 'http://t.weather.sojson.com/api/weather/city/101200101'; // 城市编码修改为自己所在城市的
# 城市编码数据可以到这里查询https://gitee.com/csitian_admin_admin/dytq/wikis/%E5%9F%8E%E5%B8%82%E7%BC%96%E7%A0%81
$tianqiapi = https_request($tianqiurl);
$tianqi = json_decode($tianqiapi, true);

$data = array(
    'event' => 'SendTextMsg',
    'robot_wxid' => '机器人微信号',
    'to_wxid' => '接收人微信号',
    "msg" =>   $name."-早上好\r" .
    "今天：" .  $tianqi['data']['forecast']['0']['type'].','.$tianqi['data']['forecast']['0']['low'].'-'.$tianqi['data']['forecast']['0']['high']. "\r" .
    $yjh . "\r" .
    $wxts . "\r" .
    $qinghuaapi . "\r"
);
$jsonStr = json_encode($data);


// 为了避免不要的麻烦，这块限制早上6点到12点才能执行，如果测试的话，只需要改一改6和12！！全天就是0和24
if (date('H') >= 6 && date('H') <= 12) {
    $send = http_post_json($url, $jsonStr);
}
?>